
<?php

class API {
    public function printFullName($fullName) {
        echo "Full name: $fullName <br>" . PHP_EOL;
    }

    public function printHobbies(array $hobbies) {
        echo "Hobbies:" . PHP_EOL;
        foreach ($hobbies as $hobby) {
            echo "    $hobby" . PHP_EOL;
        }
    }

    public function printPersonalInfo($info) {
        echo "Age: {$info->age}" . PHP_EOL;
        echo "Email: {$info->email}" . PHP_EOL;
        echo "Birthday: {$info->birthday}" . PHP_EOL;
    }
}

// Creating an instance of the API class
$api = new API();

// Taking input from the user
echo "Enter your full name: ";
$fullName = trim(fgets(STDIN));

echo "Enter your hobbies (comma-separated): ";
$hobbiesInput = trim(fgets(STDIN));
$sampleHobbies = explode(",", $hobbiesInput);

echo "Enter your age: ";
$age = trim(fgets(STDIN));

echo "Enter your email: ";
$email = trim(fgets(STDIN));

echo "Enter your birthday: ";
$birthday = trim(fgets(STDIN));

// Creating the object for personal info
$personalInfo = (object) array(
    'age' => $age,
    'email' => $email,
    'birthday' => $birthday
);

// Calling the functions with user-provided input
$api->printFullName($fullName);
$api->printHobbies($sampleHobbies);
$api->printPersonalInfo($personalInfo);

?>
